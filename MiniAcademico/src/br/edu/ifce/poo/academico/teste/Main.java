package br.edu.ifce.poo.academico.teste;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.edu.ifce.poo.academico.Professor;

/*
 * Funcionalidades:
 * 1 - cadastrar professor;
 * 2 - exibir professores;
 * 3 - cadastrar disciplina;
 * 4 - exibir disciplinas; 
 */


public class Main {
	private List<Professor> professores;
	
	private Main() {
		professores = new ArrayList<>();
	}
	
	private void cadastrarProfessor() {
		String nome = JOptionPane.
				showInputDialog("Nome do professor:");
		String cpf = JOptionPane.
				showInputDialog("CPF do professor:");
		String area = JOptionPane.
				showInputDialog("Area de atuação:");
		Professor p = new Professor(nome, cpf, area);
		professores.add(p);
	}
	
	private void mainLoop() {
		String comando = JOptionPane.
				showInputDialog("Qual o comando?");
		switch (comando.toLowerCase()) {
		case "cadastrar professor":
			cadastrarProfessor();
			break;
		case "sair":
			System.exit(0);
			break;
		}
		
	}

	public static void main(String[] args) {
		Main m = new Main();
		while(true) {
			m.mainLoop();
		}
	}

}
