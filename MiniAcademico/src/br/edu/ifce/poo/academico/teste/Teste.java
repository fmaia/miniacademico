package br.edu.ifce.poo.academico.teste;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifce.poo.academico.Aluno;
import br.edu.ifce.poo.academico.Professor;
import br.edu.ifce.poo.academico.Usuario;

public class Teste {
	public static void main(String[] args) {
		Aluno a = new Aluno("João", "111.222.333-44",
							1, "Maria");
		Professor p = new Professor("Marta", "222.111.333-55",
							"Sistemas de Computação");
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(a);
		usuarios.add(p);
		usuarios.get(0).imprimirDados();
		usuarios.get(1).imprimirDados();
	}

}







