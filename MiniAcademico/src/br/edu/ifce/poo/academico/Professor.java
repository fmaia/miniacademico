package br.edu.ifce.poo.academico;

public class Professor extends Usuario{
	private String subarea;
		
	public Professor(){
		super();
		System.out.println("Um novo professor foi criado");
	}
	public Professor(String nome, String cpf, 
			String  subarea){
		super(nome, cpf);
		this.subarea = subarea;
		
	}
	public void setSubarea(String subarea) {
		this.subarea = subarea;
	}
	public String getSubarea() {
		return this.subarea;
	}
	public void imprimirDados() {
		System.out.println("Nome: " + this.getNome());
		System.out.println("CPF: " + this.getCpf());
		System.out.println("Subarea: " + this.subarea);
	}	
	public String toString() {
		return "[" + this.getCpf() + "]" + this.getNome() + 
				" - " + this.getSubarea();
	}
}
