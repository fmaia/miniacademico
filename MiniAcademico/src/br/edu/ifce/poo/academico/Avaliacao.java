package br.edu.ifce.poo.academico;

public class Avaliacao {
	private int codigo;
	private String descricao;
	private Diario diario;
	private double[] notas;
	
	public Avaliacao() {	
	}
	public Avaliacao(int codigo, String descricao, 
					 Diario diario, double[] notas) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.diario = diario;
		this.notas = notas;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCodigo() {
		return this.codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return this.descricao;
	}
	public void setDiario(Diario diario) {
		this.diario = diario;
	}
	public Diario getDiario() {
		return this.diario;
	}
	public void setNotas(double[] notas) {
		this.notas = notas;
	}
	public double[] getNotas() {
		return this.notas;
	}
	public String toString() {
		String s = "";
		s += "[" + codigo + "] ";
		s += descricao + " : ";
		for(int k = 0; k < notas.length; k++) {
			s += diario.getAlunos()[k].getNome() 
					+ ": " + notas[k];
			s += "\n";
		}
		return s;
	}
	
}





