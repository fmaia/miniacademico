package br.edu.ifce.poo.academico;

public class Aluno extends Usuario{
	private int matricula;
	private String nomeMae;
	public Aluno (){
		super();
		System.out.println("Um aluno novo foi criado");
	}	
	public Aluno(String nome, String cpf, int matricula,String nomeMae){
		super(nome, cpf);
		System.out.println("Usou o construtor especial.");
		this.matricula = matricula;
		this.nomeMae = nomeMae;
	}
	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}
	public String getNomeMae() {
		return this.nomeMae;
	}
	public void imprimirDados() {
		System.out.println("Nome: " + this.getNome());
		System.out.println("CPF: " + this.getCpf());
		System.out.println("Matricula: " + this.matricula);
		System.out.println("Nome mãe: " + this.nomeMae);
	}
	
	public void gerarMatricula() {
		if(matricula == 0) {
		this.matricula = new java.util.Random().nextInt();
		}
	}
	
	public int getMatricula() {
		return matricula;
	}
	
	
	public String toString() {
		return"["+ this.getCpf() + "]"+this.getNome()+" - " + matricula + nomeMae;
	
	}
}
