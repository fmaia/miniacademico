package br.edu.ifce.poo.academico;

public class Diario {
	private int codigo;
	private Professor formador;
	private Disciplina disciplina;
	private Aluno[] alunos;
	
	public Diario (){
	
	}	
	public Diario(int codigo,Professor formador, Disciplina disciplina, Aluno[] alunos){
		this.codigo = codigo;
		this.formador = formador;
		this.disciplina = disciplina;
		this.alunos = alunos;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCodigo() {
		return this.codigo;
	}
	public void setFormador(Professor formador) {
		this.formador = formador;
	}
	public Professor getFormador() {
		return this.formador;
	}
	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}
	public Disciplina getDisciplina() {
		return this.disciplina;
	}
	public void setAlunos(Aluno[] alunos) {
		this.alunos = alunos;
	}
	public Aluno[] getAlunos() {
		return this.alunos;
	}

	public String toString() {
		String s = "";
		s +="["+codigo+"]";
		s += "Professor: " + formador + ";";
		s += "Disciplina: " + disciplina + ";";
		s += "Alunos:  {";
		for(int k = 0; k < alunos.length; k++) {
			s += alunos[k] + ";";
		}
		s += "}";
		return s;
	
	}

}


