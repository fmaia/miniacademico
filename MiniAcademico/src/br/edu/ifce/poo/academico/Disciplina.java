package br.edu.ifce.poo.academico;

public class Disciplina {
	private int codigo;
	private String sigla;
	private String nome;
	
	public Disciplina (){
		System.out.println("Disciplina criada. ");
	}
	public Disciplina(int codigo, String sigla, String nome){
		this.codigo = codigo;
		this.sigla = sigla;
		this.nome = nome;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getCodigo() {
		return this.codigo;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getSigla() {
		return this.sigla;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return this.nome;
	}
	public String toString() {
		return"["+ codigo + "]"+sigla+" - " + nome;
	}
}





